/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
  SafeAreaView,
  Text
} from 'react-native';
import Route from './src/route'
import { Provider } from 'react-redux'
import store from './src/redux/store'

class App extends React.Component{
  render() {
    return (
      <Provider store={store}>
        <SafeAreaView style={{
            flex: 1,
            backgroundColor: '#fff',
          }}>
              <Route />
        </SafeAreaView>
      </Provider>
    )
  };
}

export default App;
