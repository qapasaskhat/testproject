import initialState from '../redux/initialState';

export const appReducer = (state = initialState.app, action) => {
  switch (action.type) {
    case 'AUTH':
      return {
        ...state,
        auth: true,
      };
    case 'GET_TOKEN':
      return {
        ...state,
        token: action.payload.token,
        user: action.payload.user
      };
    case 'LOG_OUT':
        return {
            ...state,
            auth: false,
            token: '',
        }
    default:
      return state;
  }
};
