import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import {Button} from '../../components'

class ProfileScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }
  logout=()=>{
    this.props.navigation.replace('Auth')
    this.props.dispatch({type: 'LOG_OUT'})
  }
  render() {
    return (
      <View style={styles.container}>
        <Text> Profile </Text>
        <View style={styles.absolute}>
          <Button title='Выйти' onPress={()=>{this.logout()}} />
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor: '#fff'
    },
    absolute:{
      position: 'absolute',
      width: '100%',
      //height: '100%',
      bottom: 20
    }
})

const mapStateToProps = (state) => ({
  auth: state.appReducer.auth,
});
const mapDispatchToProps = (dispatch) => ({
  dispatch,
});
export default connect(mapStateToProps,mapDispatchToProps) (ProfileScreen);
