import React, { Component } from 'react';
import { View, Text, ActivityIndicator } from 'react-native';
import { connect } from 'react-redux';

class AppLoader extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }
  componentDidMount=()=>{
      console.log(this.props)
      if(this.props.auth){
        this.props.navigation.navigate('TabMain')
      } else {
        this.props.navigation.navigate('Auth')
      }
  }
  render() {
    return (
      <View style={{
          flex: 1,
          backgroundColor: '#fff',
          justifyContent: 'center',
          alignItems: 'center'
      }}>
        <ActivityIndicator color='#009933' size={'large'} />
      </View>
    );
  }
}
const mapStateToProps = (state) => ({
    auth: state.appReducer.auth,
  });
  const mapDispatchToProps = (dispatch) => ({
    dispatch,
  });
export default connect(mapStateToProps,mapDispatchToProps) (AppLoader);
