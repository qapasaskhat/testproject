import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { Button, Input } from '../../components'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { connect } from 'react-redux';

class AuthScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      login: '',
      password: ''
    };
  }
  componentDidMount=()=>{
    console.log(this.props)
      if(this.props.auth){
        this.props.navigation.navigate('TabMain')
      } 
  }
  login=()=>{
    this.props.navigation.replace('TabMain')
    this.props.dispatch({type: 'AUTH'})
  }

  render() {
    return (
      <KeyboardAwareScrollView style={styles.container}>
        <View style={styles.view} />
        <Input 
          title='Login' placeholder='login'
          changeText={(value)=>{this.setState({login: value})}}
          value={this.state.login} />
        <Input 
          title='Password' placeholder='password'
          changeText={(value)=>{this.setState({password: value})}}
          secureTextEntry={true}
          value={this.state.password} />
        <Button title={'Enter'} onPress={()=>{
          this.login()
        }}/>
      </KeyboardAwareScrollView>
    );
  }
}
const styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor: '#fff',
    },
    view:{
      width: 200,
      height: 200,
      alignSelf: 'center',
      borderRadius: 7,
      backgroundColor: '#eee',
      marginTop: 20
    }
})
const mapStateToProps = (state) => ({
  auth: state.appReducer.auth,
});
const mapDispatchToProps = (dispatch) => ({
  dispatch,
});
export default connect(mapStateToProps,mapDispatchToProps) (AuthScreen);
