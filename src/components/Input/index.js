import React from 'react'
import {
    View, Text, TextInput, StyleSheet
} from 'react-native'

const Input = ({title,placeholder, value, changeText, secureTextEntry}) =>(
    <View style={styles.container}>
        <Text style={styles.text}>{title}</Text>
        <TextInput 
            style={styles.input}
            value={value}
            onChangeText={changeText}
            secureTextEntry={secureTextEntry}
            placeholder={placeholder} />
    </View>
)
const styles = StyleSheet.create({
    container:{
        marginHorizontal: 20,
        marginVertical: 10
    },
    text:{
        fontSize: 15,
    },
    input:{
        paddingHorizontal: 10,
        paddingVertical: 12,
        borderWidth: 1,
        borderColor: '#eee',
        borderRadius: 7,
    }
})
export default Input