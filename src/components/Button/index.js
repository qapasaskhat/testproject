import React from 'react'
import {
    Text, TouchableOpacity, StyleSheet
} from 'react-native'

const Button = ({title, onPress})=>(
    <TouchableOpacity onPress={onPress} style={styles.button}>
        <Text style={styles.text}>{title}</Text>
    </TouchableOpacity>
)
const styles = StyleSheet.create({
    button:{
        marginHorizontal: 20,
        paddingVertical: 12,
        borderRadius: 7,
        backgroundColor: '#009933'
    },
    text:{
        fontSize: 15,
        textAlign: 'center',
        lineHeight: 18,
        color: '#fff',
        fontWeight: '600',
        letterSpacing: 1
    }
})
export default Button