import React from 'react'
import { Image } from 'react-native'
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs'
import {createDrawerNavigator} from '@react-navigation/drawer'
import {NavigationContainer} from '@react-navigation/native'
import {createStackNavigator} from '@react-navigation/stack'

import { MainScreen, AuthScreen, SettingsScreen, ProfileScreen, AppLoader } from '../screens'
import { ic_grid, ic_user } from '../assets'
const Stack = createStackNavigator();
const Tabs = createBottomTabNavigator();
const Drawer = createDrawerNavigator();

const TabNavigation = ()=>(
    <Tabs.Navigator 
    tabBarOptions={{
        activeTintColor:'#009933',
        inactiveTintColor:'#010005',
      }}>
        <Tabs.Screen options={{
            tabBarIcon: (tintcolor)=>(
                <Image source={ic_grid} style={{
                    width: 24, height: 24, resizeMode: 'contain', tintColor: tintcolor.color
                }} />
            )
        }} name='Main' component={MainScreen} />
        <Tabs.Screen options={{
            tabBarBadge: 2,
            tabBarIcon: ()=>(
                <Image source={ic_user} style={{
                    width: 24, height: 24, resizeMode: 'contain'
                }} />
            )
        }} name='Profile' component={ProfileScreen}/>
    </Tabs.Navigator>
)

const DrawerNavigator = ()=>(
    <Drawer.Navigator>
        <Drawer.Screen name='Drawer' component={TabNavigation} />
        <Drawer.Screen name='Settings' component={SettingsScreen}/>
    </Drawer.Navigator>
)

const StackNavigation = ()=>(
    <Stack.Navigator headerMode='none' initialRouteName='AppLoader' >
        <Stack.Screen name='AppLoader' component={AppLoader} />
        <Stack.Screen name='Auth' component={AuthScreen}/>
        <Stack.Screen name='TabMain' component={DrawerNavigator}/>
    </Stack.Navigator>
)

const Route = () =>(
    <NavigationContainer>
        <StackNavigation />
    </NavigationContainer>
)
export default Route